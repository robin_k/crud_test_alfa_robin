
"use strict";

var mysql      = require('mysql');
// var mysql = require('promise-mysql');
var config    = require('config');
var dbConfig = config.get('dbConfig');
var connection = mysql.createConnection({
  host     : dbConfig.host,
  user     : dbConfig.username,
  password : dbConfig.password,
  database : dbConfig.database
});

connection.connect(function(err) {
  if (err) {
    console.error('error connecting: ' + err.stack);
    return;
  }
   
  console.log('connected as id ' + connection.threadId);
});



module.exports = connection;