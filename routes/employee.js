var mysql      = require('mysql');
var _ = require('underscore');
var Promise = require("bluebird");
var con = require('../lib/connection');
var config    = require('config');
var dbConfig = config.get('dbConfig');

module.exports = function(app) {

    app.get('/employee', function(req,res,next) {
        var q = "select e.id, e.full_name, e.address, e.dob, e.role_id, r.role_name, s.salary from employee e inner join role r on r.role_id = e.role_id inner join salary s on s.id = e.id";

        con.query(q, function (error, results, fields) {
            if (error) {
                con.release;
                res.json(error);
            } else {
                con.release;
                res.json(results);
            }
        });
    });

    app.get('/employee/:empId', function(req,res,next) {
        var param = req.params.empId;
        var q = "select e.id, e.full_name, e.address, e.dob, e.role_id, r.role_name, s.salary from employee e inner join role r on r.role_id = e.role_id inner join salary s on s.id = e.id where e.id = "+param;
        
        con.query(q, function (error, results, fields) {
            if (error) {
                con.release;
                res.json(error);
            } else {
                con.release;
                res.json(results);
            }
        });
    });

    function separateObj (req,res,next) {
        var objSalary = objEmp = objDob =  {};
        _.each(req.body, function(val, key){
            if (key == 'salary') {
                objSalary = {
                    [key] : val
                }
            } else {
                if (key == 'dob') {
                    // dob = "STR_TO_DATE('"+val+"','%Y-%m-%dT%TZ')"; e.. . .. 
                    objDob = {
                        [key] :val
                    }
                } else {
                    _.extend(objEmp, {
                        [key] : val
                    });
                }
            }
        });
        req.objEmp = objEmp;
        req.objDob = objDob;
        req.objSalary = objSalary;
        req.objAllEmp = _.extend(objEmp, objDob);
        next();
    }

    function isBodyEmpty(req,res,next) {
        if (_.isEmpty(req.body)) {
            res.json({
                "message" : 'body is empty'
            });
        } else {
            next();
        }
    }

    app.post('/employee/:empId', isBodyEmpty, separateObj, function(req,res,next) {
        var empId = req.params.empId;
        var objEmp = req.objEmp;
        var objSalary = req.objSalary;
        var objDob = req.objDob;
        var conAsync = mysql.createConnection({
            host     : dbConfig.host,
            user     : dbConfig.username,
            password : dbConfig.password,
            database : dbConfig.database
        });
        
        Promise.promisifyAll(conAsync);
        return conAsync.connectAsync()
        .then(conAsync.beginTransactionAsync())
        .then(() => {
            const queryPromises = [];
            if (!_.isEmpty(objDob)) {
                var dob = objDob.dob;
                var q1 = conAsync.queryAsync("UPDATE employee SET dob = STR_TO_DATE('"+dob+"','%Y-%m-%dT%TZ') WHERE id = "+empId);
                queryPromises.push(q1);
            }
            if (!_.isEmpty(objEmp)) {
                var q2 = conAsync.queryAsync("UPDATE employee SET ? WHERE id = "+empId, objEmp);
                queryPromises.push(q2);
            }
            if (!_.isEmpty(objSalary)) {
                var q3 = conAsync.queryAsync('UPDATE salary SET ? WHERE employee_id ='+empId, objSalary);
                queryPromises.push(q3);
            }    
            return Promise.all(queryPromises);
        })
        .then(results => {
            return conAsync.commitAsync()
            .then(conAsync.endAsync())
            .then(() => {
                res.json(results);
            });
        })
        .catch(err => {
            return conAsync.rollbackAsync()
            .then(conAsync.endAsync())
            .then(() => {
                res.json(err);
            });
        });
    });

    app.put('/employee', isBodyEmpty, separateObj, function(req,res,next) {
        con.release; //remove intial connection
        var objSalary = req.objSalary;
        var objAllEmp = req.objAllEmp;
        con.query("INSERT INTO employee (full_name, address, dob, role_id) VALUES('"+objAllEmp.full_name+"', '"+objAllEmp.address+"', STR_TO_DATE('"+objAllEmp.dob+"','%Y-%m-%dT%TZ'), "+objAllEmp.role_id+")",  
            function (error, results, fields) {
                if (error) {
                    return con.rollback(function() {
                        res.json(error);
                    });
                } else {
                    var empId = null;
                    _.each(results, function(val, key){
                        if (key == 'insertId') {
                            console.log(val);
                            empId = val;
                        }
                    });
                    con.query("INSERT INTO salary (employee_id, salary) VALUES("+empId+", "+objSalary.salary+")",  
                        function (error, results, fields) {
                            if (error) {
                                return con.rollback(function() {
                                    res.json(error);
                                });
                            } else {
                                return res.json({
                                    "message" : "sukses"
                                });
                            }
                    });
                }
        });
    });

    app.delete('/employee/:empId', function(req,res,next){
        var empId = req.params.empId;
        con.query('DELETE FROM employee WHERE id = '+empId,  function (error, results, fields) {
            if (error) {
                return con.rollback(function() {
                    res.json(error);
                });
            } else {
                return res.json({
                    "message" : "sukses"
                });
            }
        });
    });
}